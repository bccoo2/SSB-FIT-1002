/*
 * Copyright (c) 2016. B.Cooper
 */

import java.util.Scanner;

public class Main {

    // program signature
    public static void main(String[] args) {
        Scanner oScan = new Scanner(System.in);
        System.out.print("Enter your Surname: ");
        String sSurname = oScan.next();
        System.out.print("Enter your Name: ");
        String sName = oScan.next();

        System.out.print("Enter your Mothers Maiden Name: ");
        String sMName = oScan.next();
        System.out.print("Enter the name of the place you were born: ");
        String sPOB = oScan.next();

        String sSecretNameA = sName.substring(0, 2) + sSurname.substring(0, 3);
        String sSecretNameB = sPOB.substring(0, 3) + sMName.substring(0, 2);
        System.out.print("\nYour secret code name is: " + sSecretNameA + " " + sSecretNameB);

    }

}
